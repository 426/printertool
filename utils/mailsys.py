
import smtplib

from .cred import *

# https://gist.github.com/mat100/5f7d0d7afe37f3e57347f976f742f09d

class Emailer:
    def sendmail(self, recipient, subject, content):
        headers = ["From: " + GMAIL_USERNAME, "Subject: " + subject, "To: " + recipient,
                   "MIME-Version: 1.0", "Content-Type: text/html"]
        headers = "\r\n".join(headers) 
        session = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
        session.ehlo()
        session.starttls()
        session.ehlo() 
        session.login(GMAIL_USERNAME, GMAIL_PASSWORD) 
        session.sendmail(GMAIL_USERNAME, recipient, headers + "\r\n\r\n" + content)
        session.quit
 
sender = Emailer()
 


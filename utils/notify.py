from utils.mailsys import sender
import requests

DEV = False
ANDI = 'abren426@gmail.com'
GRUBERSERVICE = 'service@gruber-office.it'
GRUBEROFFICE = 'office@gruber-office.it'
# //////////////////////////////////////////////////////////////////
# //////////////////////////////////////////////////////////////////
# MAIL SIGNALS

def get_this_ip():
    try: 
        ip = str(requests.get('https://api.ipify.org').text)
    except Exception as e:
        print('get_ip', str(e))
        ip = ''
    return ip

def send_error_mail(mex):
    try:
        sendTo = ANDI
        emailSubject = get_this_ip() + " Raspberry error"
        emailContent = mex
        sender.sendmail(sendTo, emailSubject, emailContent)  
    except Exception as e:
        print(str(e))
        pass


def send_activation_mail(public_ip, private_ip):
    try:
        if DEV == True:
            sendTo = ANDI
        else:
            sendTo = GRUBERSERVICE
        emailSubject = get_this_ip() + " Raspberry ist online"
        emailContent = "Raspberry ist online. " + "Pub./Priv. IP: "+ str(public_ip) + " / " + str(private_ip)
        sender.sendmail(sendTo, emailSubject, emailContent)  
    except:
        pass


def send_netscan_mail(data):
    try:
        if DEV == True:
            sendTo = ANDI
        else:
            sendTo = GRUBEROFFICE
        emailSubject = get_this_ip() + " Raspberry netscan"
        emailContent = str(data)
        sender.sendmail(sendTo, emailSubject, emailContent)  
    except:
        pass

def send_low_toner_mail(data):
    try:
        if DEV == True:
            sendTo = ANDI
        else:
            sendTo = GRUBEROFFICE
        emailSubject = "Tonerstand niedrig"
        emailContent = str(data)
        sender.sendmail(sendTo, emailSubject, emailContent)  
    except:
        pass


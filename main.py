# ------------------------------------------------------------------
# ///////////////////////PrinterTool////////////////////////////////
# ///////////////////////Version 0.2////////////////////////////////
# DANIELLLLLLLLLLLLLLLLLLLLLLLL
# ------------------------------------------------------------------
import os
import json
import getmac
import sys
import time
import schedule
import logging
import requests
from datetime import datetime
# ------------------------------------------
from app.printer_api import get_printer_data
from app.odoo_api import post_printer_data
from utils.notify import send_activation_mail, send_error_mail
from utils.cred import *
# ------------------------------------------------------------------
# //////////////////////////////////////////////////////////////////
# APP


# Every 5 minutes signal to odoo: if response = "data requested": send data else: continue

def send_signal():
    session = requests.Session()
    r = session.post(url=ODOO_AUTH_ENDPOINT, data=json.dumps(ODOO_AUTH_CREDENTIALS), headers=ODOO_API_HEADERS)
    if r.ok:
        try:
            result = r.json()['result']
            if result.get('session_id'):
                session.cookies['session_id'] = result.get('session_id')
            r = session.post(url=ODOO_DATA_REQUEST_URL, data=json.dumps({'mac':getmac.get_mac_address()}), headers=ODOO_API_HEADERS)
            if r.status_code != 200:
                print('odoo post error')
                try:
                    print(r.json())
                except:
                    pass
            else:
                print('RES FROM ODOO:', r.json())
                res = r.json()
                req = res['result']
                if req['update_requested'] == False:
                    print('no update reqested')
                else:
                    print('update reqested!')
                    app()
        except:pass
    else:
        print('odoo auth error')




def app():
    print(getmac.get_mac_address())
    # get printer data
    printer_data = get_printer_data()
    if printer_data and printer_data != False:
        posted = post_printer_data(printer_data)
        if posted == 777:
            send_error_mail('Odoo auth error')
        elif posted == 888:
            send_error_mail('Odoo post error')
        else:
            send_error_mail(str(posted))
    else:
        send_error_mail('No printer data')
    now = datetime.now()
    nowstr = str(now.strftime("%d/%m/%Y %H:%M:%S"))
    logging.info(f'Finished: {nowstr}')
# ------------------------------------------------------------------
# //////////////////////////////////////////////////////////////////
# INIT

if 'signal' in sys.argv:
    logging.basicConfig(filename='myapp.log', level=logging.INFO)
    now = datetime.now()
    nowstr = str(now.strftime("%d/%m/%Y %H:%M:%S"))
    logging.info(f'Started: {nowstr}')
    send_signal()

def get_ip_address():
    import socket
    ip_address = ''
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8",80))
    ip_address = s.getsockname()[0]
    s.close()
    return ip_address



if 'start' in sys.argv:
    try:
        public_ip = requests.get('https://api.ipify.org').text or 'Unable to get publ. IP'
    except Exception as e:
        public_ip = str(e)
    try:
        private_ip = get_ip_address()
    except Exception as e:
        private_ip = str(e)

    send_activation_mail(public_ip, private_ip)
    logging.basicConfig(filename='myapp.log', level=logging.INFO)
    now = datetime.now()
    nowstr = str(now.strftime("%d/%m/%Y %H:%M:%S"))
    logging.info(f'Started: {nowstr}')
    app()

elif 'stop' in sys.argv:
    exit()

elif 'update' in sys.argv:
    # fetch_update() #TODO
    exit()

else:
    usg = """
    PrinterTool CLI Usage
    ----------------------
    Start:    "main.py start", 
    Stop:     "main.py stop",
    Update:   "main.py update"
    """
    print(usg)
    exit()
# //////////////////////////////////////////////////////////////////
# ------------------------------------------------------------------

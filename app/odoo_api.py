# POST TO ODOO
import requests
import json
import getmac

from utils.cred import *
from utils.notify import send_activation_mail, send_error_mail, send_netscan_mail

def post_printer_data(printer_data):
    session = requests.Session()
    r = session.post(url=ODOO_AUTH_ENDPOINT, data=json.dumps(ODOO_AUTH_CREDENTIALS), headers=ODOO_API_HEADERS)
    if r.ok:
        try:
            result = r.json()['result']
            if result.get('session_id'):
                session.cookies['session_id'] = result.get('session_id')
            print(printer_data)
            r = session.post(url=ODOO_PRINTER_ENDPOINT, data=json.dumps(printer_data), headers=ODOO_API_HEADERS)
            if r.status_code != 200:
                return 888
            else:
                print('RES FROM ODOO:', r.json())
                return r.json()
        except:pass
    else:
        return 777



import chardet
import requests
import socket
import getmac
import multiprocessing
from puresnmp import get
from utils.oids import *
from multiprocessing import Pool
# ------------------------------------------
from utils.mailsys import sender
from utils.notify import send_activation_mail, send_error_mail, send_netscan_mail, send_low_toner_mail
# ------------------------------------------------------------------
# //////////////////////////////////////////////////////////////////
# GET MY IP
def get_my_ip():
    try:
        current_ip = [l for l in ([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")][:1], [[(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) if l][0][0]
        p1 = current_ip.split('.')[0]
        p2 = current_ip.split('.')[1]
        p3 = current_ip.split('.')[2]
        my_ip = p1 + '.' + p2 + '.' + p3 + '.'
        return my_ip
    except Exception as e:
        ex = 'Unable to get IP: ' + str(e)
        send_error_mail(ex)
        return '172.16.0.'
# ------------------------------------------------------------------
# //////////////////////////////////////////////////////////////////
# GET PRINTER DATA
def get_printer_data():
    try:
        num_threads = 2 * multiprocessing.cpu_count()
        p = Pool(processes=num_threads)
        this_ip = get_my_ip()
        ip_list = [this_ip+str(x) for x in range(255)]
        printer_data = [x for x in p.imap(ping, ip_list) if x is not None]
        p.close()
        if len(printer_data) > 0:
            net_scan(printer_data)
            data_to_send = {}
            data_to_send['data'] = printer_data
            print(data_to_send)
            return data_to_send
        else:
            ex = 'len(printer_data) == 0'
            send_error_mail(ex)
    except Exception as e:
        ex = str(e)
        send_error_mail(ex)


def ping(ip):
    printer_data = talk_to_printer(ip) 
    return printer_data
# ------------------------------------------------------------------
# //////////////////////////////////////////////////////////////////
# TRANSLATE
def translate_tonerstand(t):
    if "tonercyan" in t:
        return "CYAN"
    if "tonermagenta" in t:
        return "MAGENTA"
    if "toneryellow" in t:
        return "YELLOW"
    if "tonerblack" in t:
        return "BLACK"
    else:
        return "UNKOWN"
# ------------------------------------------------------------------
# //////////////////////////////////////////////////////////////////
# NET SCAN
def net_scan(printer_data):
    data = ''
    try:
        for p in printer_data:
            data += str(p['serialnr']) + ' : ' + str(p['model']) + ', '
        if data != '':
            pass
            # send_netscan_mail(data)
    except Exception as e:
        ex = str(e)
        # send_error_mail(ex)

# ------------------------------------------------------------------
# //////////////////////////////////////////////////////////////////
# TALK

def is_old_printer(model):
    try:
        if isinstance(model, (bytes, bytearray)) and chardet.detect(model)['encoding'] == 'ascii':
            model = model.decode("ascii")
        if "SHARP" in model:
            model = model.split("SHARP")[1].strip(" ")
            is_old = model[2]
            if is_old == "M":
                return True
            else:
                return False
        else:
            return False
    except Exception as e:
        print(str(e))
        return False


def talk_to_printer(ip):
    tonerinfo = ""
    try:
        try:
            print("uptime: ", get(ip, 'private', ".1.3.6.1.2.1.1.3.0", timeout=1))   
            model = get(ip, 'private', ".1.3.6.1.2.1.25.3.2.1.3.1", timeout=1)
        except: 
            model = get(ip, 'private', ".1.3.6.1.2.1.1.1.0", timeout=1)

        if is_old_printer(model) == True:
            oids__ = OIDS_SMALL
        else:
            oids__ = OIDS_ALL

        printer_data = {}
        for k,v in oids__.items():
            try:
                # get oid value
                res = get(ip, 'private', v, timeout=1)
                if res:
                    try:
                        if isinstance(res, (bytes, bytearray)) and chardet.detect(res)['encoding'] == 'ascii':
                            print(k, ': ', res.decode("ascii"))
                            printer_data[k] = res.decode("ascii")
                        else:
                            print(k, ': ', res)
                            printer_data[k] = res
                    except: pass
                    printer_data['ip'] = requests.get('https://api.ipify.org').text or 'Unable to get IP'
                    printer_data['mac'] = getmac.get_mac_address() or 'Unable to get MAC'
                    printer_data['version'] = '1.2'

                    if 'toner' in k and res <= 10:
                        serialn = get(ip, 'private', ".1.3.6.1.2.1.43.5.1.1.17.1", timeout=1)
                        tonerinfo = "Serienn. " + str(serialn) + ": Der Tonerstand: "+ translate_tonerstand(k) + " liegt bei " + str(res) + ". "
                        send_low_toner_mail(tonerinfo)
            except: pass
        return printer_data
    except Exception as e:
        ex = str(e)
        # print(ex)
        print('IP: ', ip, ' is not a printer')


"""
https://pitstop.manageengine.com/portal/en/kb/articles/printer-status-oids
s1 = get(ip, 'private', ".1.3.6.1.2.1.25.3.2.1.5.1", timeout=1) # Up, Down or Warning
s2 = get(ip, 'private', ".1.3.6.1.2.1.25.3.5.1.1.1", timeout=1) # idle, printing, or warmup
s3 = get(ip, 'private', ".1.3.6.1.2.1.25.3.5.1.2.1", timeout=1) # lowPaper, noPaper, noToner, doorOpen, jammed, offline
"""